import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import {MainLayout} from "../components/MainLayout";
import {useState, useEffect} from 'react'
import Image from 'next/image'

export default function Index({ operators }) {
//   const [operators, setOperators] = useState([]);
//
//   useEffect(() => {
//     async function load() {
//       const response = await fetch('http://localhost:4200/operators');
//       const json = await response.json();
//       setOperators(json);
//     }
//
//     load();
//   }, [])
  
  return (
  <MainLayout>
    <h1>Оплата мобильной связи</h1>
    <h2>Выберите мобильного оператора</h2>
    <ul className={'operator-list flex'}>
      {operators.map(operator => (
        <li key={operator.id} className={'operator-item'}>
          <Link href={`/operators/[operator]`} as={`/operators/${operator.id}`}>
            <a><Image
              src={`/${operator.operator}-logo.svg`}
              width={200}
              height={100}
              alt={`${operator.operator}`}
            /></a>
          </Link>
        </li>
      ))}
    </ul>
  </MainLayout>
  )
}

Index.getInitialProps = async () => {
  const response = await fetch('http://localhost:4200/operators');
  const operators = await response.json();
  
  return {
    operators: operators //можно просто operators
  }
}