import React from 'react';
import Router, { useRouter } from "next/router";
import {MainLayout} from "../../components/MainLayout";
import {Form} from "../../components/PayForm";
import {useState, useEffect} from 'react';
import Image from "next/image";

export default function Operators({ operator: serverOperator }) {
  const [operator, setOperator] = useState(serverOperator);
  const router = useRouter()
  
  useEffect(() => {
    async function load() {
      const response = await fetch(`http://localhost:4200/operators/${router.query.operator}`);
      const data = await response.json();
      setOperator(data)
    }
    
    if (!operator) {
      load()
    }
  }, [])
  
  if (!operator) {
    return (
      <MainLayout title={'Loading'}>
        <p>Загрузка ...</p>
      </MainLayout>
    )
  }
  return (
  <MainLayout title={operator.title} className={'operator-pay'}>
    <Form>
      <h1 className={'hide'}>{operator.title}</h1>
      <Image
        src={`/${operator.operator}-logo.svg`}
        width={200}
        height={100}
        alt={`${operator.operator}`}
      />
    </Form>
  </MainLayout>
  )
}

Operators.getInitialProps = async ({query, req}) => {
  if (!req) {
    return (
      {operator: null}
    )
  }
  const response = await fetch(`http://localhost:4200/operators/${query.operator}`);
  const operator = await response.json();
  
  return {
    operator
  }
}