import React from 'react'
import Head from 'next/head'

export function MainLayout({ children, title = 'Оплата связи' }) {
  return (
    <React.Fragment>
      <Head>
        <title>Develops | {title}</title>
        <meta charSet="utf-8"/>
        <meta name="keywords" content="develops, mobile"/>
      </Head>
      <main className={'flex container'}>
        {children}
      </main>
    </React.Fragment>
  )
}