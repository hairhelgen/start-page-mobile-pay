import React from 'react'
import Router from "next/router";

export function Form({ children }) {
  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const data = {
      tel: event.target.tel.value,
      amount: event.target.amount.value
    }
    
    const JSONdata = JSON.stringify(data);
    
    const response = await fetch('/api/form', {
      body: JSONdata,
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    })
    const result = await response.json()
    alert(result.data);
  }
  
  return (
    <React.Fragment>
      {children}
      <form className={'flex form'} onSubmit={handleSubmit}>
      {/*<form className={'flex form'} action={'/api/form'} method={'post'}>*/}
        <label htmlFor={'tel'} className={'form__label'}>Введите номер телефона</label>
        <input
          type={'tel'}
          id={'tel'}
          name={'tel'}
          className={'form__input'}
          placeholder={'+7(ххх)ххх-хх-хх'}
          maxLength={10}
          minLength={10}
          required
        />
        <label htmlFor={'amount'} className={'form__label'}>Введите сумму для зачисления</label>
        <input
          type={'number'}
          id={'amount'}
          name={'amount'}
          className={'form__input'}
          min={1}
          max={1000}
          placeholder={'от 1 до 1000 руб'}
          pattern={'0, 9'}
          title={'Сумма должна состять из цифр (от 0 до 9)'}
          required
        />
        <button className={'form__submit'} type={'submit'}>Отправить</button>
      </form>
      <button
        onClick={() => Router.push('/')}
        className={'btn-back'}
      >
        Вернуться
      </button>
    </React.Fragment>
  )
}